<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <parent>
    <artifactId>acs-aem-commons</artifactId>
    <groupId>com.adobe.acs</groupId>
    <version>4.2.0</version>
  </parent>
  <modelVersion>4.0.0</modelVersion>
  <artifactId>acs-aem-commons-bundle</artifactId>
  <packaging>bundle</packaging>
  <name>ACS AEM Commons Bundle</name>
  <description>Main ACS AEM Commons OSGi Bundle. Includes commons utilities.</description>
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.felix</groupId>
        <artifactId>maven-bundle-plugin</artifactId>
        <extensions>true</extensions>
        <executions>
          <execution>
            <id>generate-scr-metadata-for-unittests</id>
            <phase>process-classes</phase>
            <goals>
              <goal>manifest</goal>
            </goals>
            <configuration>
              <exportScr>true</exportScr>
            </configuration>
          </execution>
        </executions>
        <configuration>
          <instructions>
            <Bundle-Activator>com.adobe.acs.commons.util.impl.Activator</Bundle-Activator>
            <Bundle-SymbolicName>com.adobe.acs.acs-aem-commons-bundle</Bundle-SymbolicName>
            <Import-Package>com.adobe.cq.dialogconversion;resolution:=optional,
                            com.amazonaws.*;resolution:=optional, 
                            com.github.jknack.handlebars;resolution:=optional, 
                            com.day.cq.replication;version="[6.4,7)", 
                            org.apache.poi.ss.usermodel;version="[3.0,6)",  
                            org.apache.poi.ss.util;version="[3.0,6)",
                            org.apache.poi.xssf.usermodel;version="[2.0,6)",
                            twitter4j*;version="[3.0.5,4)";resolution:=optional,
                            org.apache.sling.xss;version="[1.1,3)", 
                            com.github.benmanes.caffeine*;resolution:=optional,
                            
                            javax.annotation;version=0.0.0,
                            !com.google.errorprone.annotations,
                            !com.google.errorprone.annotations.concurrent,
                            !org.bouncycastle.jce.*,
                            !org.checkerframework.checker.nullness.qual,
                            !android.util.*,
                            
                            !com.google.common.*,
                            !com.google.thirdparty.*,
                            !com.jcraft.*,
                            !io.jsonwebtoken.*,
                            *</Import-Package>
            <Sling-Model-Packages>com.adobe.acs.commons.redirectmaps.models,
                            com.adobe.acs.commons.version.model,
                            com.adobe.acs.commons.wcm.comparisons.model,
                            com.adobe.acs.commons.workflow.bulk.execution.model,
                            com.adobe.acs.commons.mcp.model,
                            com.adobe.acs.commons.reports.models</Sling-Model-Packages>
          </instructions>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-antrun-plugin</artifactId>
        <version>1.8</version>
        <executions>
          <execution>
            <id>backup-scr-files</id>
            <phase>process-classes</phase>
            <goals>
              <goal>run</goal>
            </goals>
            <configuration>
              <target>
                <move>
                  <fileset />
                </move>
              </target>
            </configuration>
          </execution>
          <execution>
            <id>merge-scr-files</id>
            <phase>generate-test-sources</phase>
            <goals>
              <goal>run</goal>
            </goals>
            <configuration>
              <target>
                <move>
                  <fileset />
                </move>
                <delete />
              </target>
            </configuration>
          </execution>
          <execution>
            <id>force-l10n-into-bundle</id>
            <phase>package</phase>
            <goals>
              <goal>run</goal>
            </goals>
            <configuration>
              <target>
                <jar>
                  <fileset />
                </jar>
              </target>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.felix</groupId>
        <artifactId>maven-scr-plugin</artifactId>
        <executions>
          <execution>
            <id>generate-scr-descriptor</id>
            <goals>
              <goal>scr</goal>
            </goals>
            <configuration>
              <properties>
                <service.vendor>Adobe Systems Incorporated</service.vendor>
              </properties>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.sling</groupId>
        <artifactId>maven-sling-plugin</artifactId>
        <configuration>
          <slingUrl>http://${crx.host}:${crx.port}${crx.contextRoot}/crx/repository/crx.default</slingUrl>
          <slingUrlSuffix>/apps/acs-commons/install</slingUrlSuffix>
          <deploymentMethod>WebDAV</deploymentMethod>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-failsafe-plugin</artifactId>
        <executions>
          <execution>
            <goals>
              <goal>integration-test</goal>
              <goal>verify</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <systemPropertyVariables>
            <artifactPath>${project.artifact.file.absolutePath}</artifactPath>
          </systemPropertyVariables>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>animal-sniffer-maven-plugin</artifactId>
        <executions>
          <execution>
            <phase>test</phase>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
          <showDeprecation>true</showDeprecation>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-enforcer-plugin</artifactId>
        <executions>
          <execution>
            <id>enforce-banned-dependencies</id>
            <configuration>
              <rules>
                <bannedDependencies>
                  <includes>
                    <include>com.google.guava:guava</include>
                    <include>com.google.guava:failureaccess</include>
                    <include>io.jsonwebtoken:jjwt</include>
                    <include>com.jcraft:jsch</include>
                    <include>com.jcraft:jzlib</include>
                  </includes>
                </bannedDependencies>
              </rules>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.2.1</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <createSourcesJar>true</createSourcesJar>
              <shadeSourcesContent>true</shadeSourcesContent>
              <artifactSet>
                <includes>
                  <include>com.google.guava:guava</include>
                  <include>com.google.guava:failureaccess</include>
                  <include>com.jcraft:jsch</include>
                  <include>com.jcraft:jzlib</include>
                  <include>io.jsonwebtoken:jjwt</include>
                </includes>
              </artifactSet>
              <relocations>
                <relocation>
                  <pattern>com.google.common</pattern>
                  <shadedPattern>acscommons.com.google.common</shadedPattern>
                </relocation>
                <relocation>
                  <pattern>com.google.thirdparty</pattern>
                  <shadedPattern>acscommons.com.google.thirdparty</shadedPattern>
                </relocation>
                <relocation>
                  <pattern>com.jcraft</pattern>
                  <shadedPattern>acscommons.com.jcraft</shadedPattern>
                </relocation>
                <relocation>
                  <pattern>io.jsonwebtoken</pattern>
                  <shadedPattern>acscommons.io.jsonwebtoken</shadedPattern>
                </relocation>
              </relocations>
              <filters>
                <filter>
                  <artifact>com.google.guava:guava</artifact>
                  <includes>
                    <include>com/google/**</include>
                  </includes>
                </filter>
                <filter>
                  <artifact>com.google.guava:failureaccess</artifact>
                  <includes>
                    <include>com/google/**</include>
                  </includes>
                </filter>
                <filter>
                  <artifact>com.jcraft:jsch</artifact>
                  <includes>
                    <include>com/jcraft/**</include>
                  </includes>
                </filter>
                <filter>
                  <artifact>com.jcraft:jzlib</artifact>
                  <includes>
                    <include>com/jcraft/**</include>
                  </includes>
                </filter>
                <filter>
                  <artifact>io.jsonwebtoken:jjwt</artifact>
                  <includes>
                    <include>io/jsonwebtoken/**</include>
                  </includes>
                </filter>
              </filters>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
  <dependencies>
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <version>4.5.2</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>httpcore</artifactId>
          <groupId>org.apache.httpcomponents</groupId>
        </exclusion>
        <exclusion>
          <artifactId>commons-logging</artifactId>
          <groupId>commons-logging</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient-osgi</artifactId>
      <version>4.5.2</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>httpmime</artifactId>
          <groupId>org.apache.httpcomponents</groupId>
        </exclusion>
        <exclusion>
          <artifactId>httpclient-cache</artifactId>
          <groupId>org.apache.httpcomponents</groupId>
        </exclusion>
        <exclusion>
          <artifactId>fluent-hc</artifactId>
          <groupId>org.apache.httpcomponents</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>osgi.cmpn</artifactId>
      <version>6.0.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>osgi.core</artifactId>
      <version>6.0.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.felix</groupId>
      <artifactId>org.apache.felix.scr.annotations</artifactId>
      <version>1.9.8</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>biz.aQute</groupId>
      <artifactId>bndlib</artifactId>
      <version>1.50.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>1.7.21</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>hamcrest-core</artifactId>
          <groupId>org.hamcrest</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>3.1.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.jcr</groupId>
      <artifactId>jcr</artifactId>
      <version>2.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>commons-codec</groupId>
      <artifactId>commons-codec</artifactId>
      <version>1.10</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.5</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>commons-lang</groupId>
      <artifactId>commons-lang</artifactId>
      <version>2.5</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>commons-collections</groupId>
      <artifactId>commons-collections</artifactId>
      <version>3.2.2</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-email</artifactId>
      <version>1.2</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>mail</artifactId>
          <groupId>javax.mail</groupId>
        </exclusion>
        <exclusion>
          <artifactId>activation</artifactId>
          <groupId>javax.activation</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.google.code.findbugs</groupId>
      <artifactId>jsr305</artifactId>
      <version>3.0.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>joda-time</groupId>
      <artifactId>joda-time</artifactId>
      <version>1.5.2</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.servlet.jsp</groupId>
      <artifactId>jsp-api</artifactId>
      <version>2.1</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>jstl</artifactId>
      <version>1.1.1</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.scribe</groupId>
      <artifactId>scribe</artifactId>
      <version>1.3.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>1.0.13</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>logback-core</artifactId>
          <groupId>ch.qos.logback</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.adobe.cq</groupId>
      <artifactId>cq-dialog-conversion</artifactId>
      <version>1.0.2</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.inject</groupId>
      <artifactId>javax.inject</artifactId>
      <version>1</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>3.5</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.google.code.gson</groupId>
      <artifactId>gson</artifactId>
      <version>2.3</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.felix</groupId>
      <artifactId>org.apache.felix.scr</artifactId>
      <version>1.8.2</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>animal-sniffer-annotations</artifactId>
          <groupId>org.codehaus.mojo</groupId>
        </exclusion>
      </exclusions>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>org.osgi.service.component</artifactId>
      <version>1.3.0</version>
      <scope>provided</scope>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>org.osgi.util.promise</artifactId>
      <version>1.0.0</version>
      <scope>provided</scope>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.apache.felix</groupId>
      <artifactId>org.apache.felix.framework</artifactId>
      <version>4.6.1</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.adobe.granite</groupId>
      <artifactId>com.adobe.granite.poi</artifactId>
      <version>2.0.6</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.tika</groupId>
      <artifactId>tika-core</artifactId>
      <version>1.14</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.tika</groupId>
      <artifactId>tika-parsers</artifactId>
      <version>1.5</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>metadata-extractor</artifactId>
          <groupId>com.drewnoakes</groupId>
        </exclusion>
        <exclusion>
          <artifactId>vorbis-java-tika</artifactId>
          <groupId>org.gagravarr</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netcdf</artifactId>
          <groupId>edu.ucar</groupId>
        </exclusion>
        <exclusion>
          <artifactId>apache-mime4j-core</artifactId>
          <groupId>org.apache.james</groupId>
        </exclusion>
        <exclusion>
          <artifactId>apache-mime4j-dom</artifactId>
          <groupId>org.apache.james</groupId>
        </exclusion>
        <exclusion>
          <artifactId>commons-compress</artifactId>
          <groupId>org.apache.commons</groupId>
        </exclusion>
        <exclusion>
          <artifactId>pdfbox</artifactId>
          <groupId>org.apache.pdfbox</groupId>
        </exclusion>
        <exclusion>
          <artifactId>bcmail-jdk15</artifactId>
          <groupId>org.bouncycastle</groupId>
        </exclusion>
        <exclusion>
          <artifactId>bcprov-jdk15</artifactId>
          <groupId>org.bouncycastle</groupId>
        </exclusion>
        <exclusion>
          <artifactId>poi</artifactId>
          <groupId>org.apache.poi</groupId>
        </exclusion>
        <exclusion>
          <artifactId>poi-scratchpad</artifactId>
          <groupId>org.apache.poi</groupId>
        </exclusion>
        <exclusion>
          <artifactId>poi-ooxml</artifactId>
          <groupId>org.apache.poi</groupId>
        </exclusion>
        <exclusion>
          <artifactId>geronimo-stax-api_1.0_spec</artifactId>
          <groupId>org.apache.geronimo.specs</groupId>
        </exclusion>
        <exclusion>
          <artifactId>tagsoup</artifactId>
          <groupId>org.ccil.cowan.tagsoup</groupId>
        </exclusion>
        <exclusion>
          <artifactId>asm-debug-all</artifactId>
          <groupId>org.ow2.asm</groupId>
        </exclusion>
        <exclusion>
          <artifactId>isoparser</artifactId>
          <groupId>com.googlecode.mp4parser</groupId>
        </exclusion>
        <exclusion>
          <artifactId>boilerpipe</artifactId>
          <groupId>de.l3s.boilerpipe</groupId>
        </exclusion>
        <exclusion>
          <artifactId>rome</artifactId>
          <groupId>rome</groupId>
        </exclusion>
        <exclusion>
          <artifactId>vorbis-java-core</artifactId>
          <groupId>org.gagravarr</groupId>
        </exclusion>
        <exclusion>
          <artifactId>juniversalchardet</artifactId>
          <groupId>com.googlecode.juniversalchardet</groupId>
        </exclusion>
        <exclusion>
          <artifactId>jhighlight</artifactId>
          <groupId>com.uwyn</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.amazonaws</groupId>
      <artifactId>aws-java-sdk-osgi</artifactId>
      <version>1.10.76</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>jackson-dataformat-cbor</artifactId>
          <groupId>com.fasterxml.jackson.dataformat</groupId>
        </exclusion>
        <exclusion>
          <artifactId>commons-logging</artifactId>
          <groupId>commons-logging</groupId>
        </exclusion>
        <exclusion>
          <artifactId>httpcore</artifactId>
          <groupId>org.apache.httpcomponents</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.twitter4j</groupId>
      <artifactId>twitter4j-core</artifactId>
      <version>3.0.6</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.github.ben-manes.caffeine</groupId>
      <artifactId>caffeine</artifactId>
      <version>2.6.2</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
      <version>1.10.19</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>objenesis</artifactId>
          <groupId>org.objenesis</groupId>
        </exclusion>
        <exclusion>
          <artifactId>hamcrest-core</artifactId>
          <groupId>org.hamcrest</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-api-mockito</artifactId>
      <version>1.7.4</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>powermock-api-mockito-common</artifactId>
          <groupId>org.powermock</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-module-junit4</artifactId>
      <version>1.7.4</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>powermock-module-junit4-common</artifactId>
          <groupId>org.powermock</groupId>
        </exclusion>
        <exclusion>
          <artifactId>hamcrest-core</artifactId>
          <groupId>org.hamcrest</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>junit-addons</groupId>
      <artifactId>junit-addons</artifactId>
      <version>1.4</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>xercesImpl</artifactId>
          <groupId>xerces</groupId>
        </exclusion>
        <exclusion>
          <artifactId>xmlParserAPIs</artifactId>
          <groupId>xerces</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>commons-dbcp</groupId>
      <artifactId>commons-dbcp</artifactId>
      <version>1.4</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>commons-pool</artifactId>
          <groupId>commons-pool</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.mock-server</groupId>
      <artifactId>mockserver-netty</artifactId>
      <version>3.10.4</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>xercesImpl</artifactId>
          <groupId>xerces</groupId>
        </exclusion>
        <exclusion>
          <artifactId>mockserver-client-java</artifactId>
          <groupId>org.mock-server</groupId>
        </exclusion>
        <exclusion>
          <artifactId>mockserver-core</artifactId>
          <groupId>org.mock-server</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netty-buffer</artifactId>
          <groupId>io.netty</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netty-codec</artifactId>
          <groupId>io.netty</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netty-codec-http</artifactId>
          <groupId>io.netty</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netty-common</artifactId>
          <groupId>io.netty</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netty-handler</artifactId>
          <groupId>io.netty</groupId>
        </exclusion>
        <exclusion>
          <artifactId>netty-transport</artifactId>
          <groupId>io.netty</groupId>
        </exclusion>
        <exclusion>
          <artifactId>bcprov-jdk15on</artifactId>
          <groupId>org.bouncycastle</groupId>
        </exclusion>
        <exclusion>
          <artifactId>janino</artifactId>
          <groupId>janino</groupId>
        </exclusion>
        <exclusion>
          <artifactId>mockserver-logging</artifactId>
          <groupId>org.mock-server</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-csv</artifactId>
      <version>1.6</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.commons.osgi</artifactId>
      <version>2.4.0</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.models.impl</artifactId>
      <version>1.3.0</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>commons-beanutils</artifactId>
          <groupId>commons-beanutils</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.commons.html</artifactId>
      <version>1.0.0</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>tagsoup</artifactId>
          <groupId>org.ccil.cowan.tagsoup</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.models.api</artifactId>
      <version>1.3.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.jackrabbit</groupId>
      <artifactId>jackrabbit-jcr-commons</artifactId>
      <version>2.14.0</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.jackrabbit</groupId>
      <artifactId>jackrabbit-api</artifactId>
      <version>2.16.3</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>annotations</artifactId>
          <groupId>org.jetbrains</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-imaging</artifactId>
      <version>1.0-R1534292</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>jcl-over-slf4j</artifactId>
      <version>1.7.21</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.testing.sling-mock.junit4</artifactId>
      <version>2.3.4</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>org.apache.sling.testing.sling-mock.core</artifactId>
          <groupId>org.apache.sling</groupId>
        </exclusion>
        <exclusion>
          <artifactId>org.apache.sling.testing.osgi-mock.junit4</artifactId>
          <groupId>org.apache.sling</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.testing.sling-mock</artifactId>
      <version>2.3.4</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.testing.sling-mock-jackrabbit</artifactId>
      <version>1.0.0</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>slf4j-simple</artifactId>
          <groupId>org.slf4j</groupId>
        </exclusion>
        <exclusion>
          <artifactId>org.apache.sling.commons.testing</artifactId>
          <groupId>org.apache.sling</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.testing.sling-mock-oak</artifactId>
      <version>2.1.2</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>org.apache.sling.testing.sling-mock.core</artifactId>
          <groupId>org.apache.sling</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.testing.jcr-mock</artifactId>
      <version>1.4.0</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.wcm</groupId>
      <artifactId>io.wcm.testing.aem-mock.junit4</artifactId>
      <version>2.3.2</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>slf4j-simple</artifactId>
          <groupId>org.slf4j</groupId>
        </exclusion>
        <exclusion>
          <artifactId>oak-jcr</artifactId>
          <groupId>org.apache.jackrabbit</groupId>
        </exclusion>
        <exclusion>
          <artifactId>io.wcm.testing.aem-mock.core</artifactId>
          <groupId>io.wcm</groupId>
        </exclusion>
        <exclusion>
          <artifactId>io.wcm.testing.junit-commons</artifactId>
          <groupId>io.wcm</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.skyscreamer</groupId>
      <artifactId>jsonassert</artifactId>
      <version>1.5.0</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>android-json</artifactId>
          <groupId>com.vaadin.external.google</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.hamcrest</groupId>
      <artifactId>hamcrest-library</artifactId>
      <version>1.3</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>hamcrest-core</artifactId>
          <groupId>org.hamcrest</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>me.alexpanov</groupId>
      <artifactId>free-port-finder</artifactId>
      <version>1.0</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.findify</groupId>
      <artifactId>s3mock_2.11</artifactId>
      <version>0.2.3</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>aws-java-sdk-s3</artifactId>
          <groupId>com.amazonaws</groupId>
        </exclusion>
        <exclusion>
          <artifactId>scala-library</artifactId>
          <groupId>org.scala-lang</groupId>
        </exclusion>
        <exclusion>
          <artifactId>akka-stream_2.11</artifactId>
          <groupId>com.typesafe.akka</groupId>
        </exclusion>
        <exclusion>
          <artifactId>akka-http_2.11</artifactId>
          <groupId>com.typesafe.akka</groupId>
        </exclusion>
        <exclusion>
          <artifactId>scala-xml_2.11</artifactId>
          <groupId>org.scala-lang.modules</groupId>
        </exclusion>
        <exclusion>
          <artifactId>better-files_2.11</artifactId>
          <groupId>com.github.pathikrit</groupId>
        </exclusion>
        <exclusion>
          <artifactId>scala-logging_2.11</artifactId>
          <groupId>com.typesafe.scala-logging</groupId>
        </exclusion>
        <exclusion>
          <artifactId>leveldb</artifactId>
          <groupId>org.iq80.leveldb</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.xmlunit</groupId>
      <artifactId>xmlunit-matchers</artifactId>
      <version>2.5.1</version>
      <scope>test</scope>
      <exclusions>
        <exclusion>
          <artifactId>xmlunit-core</artifactId>
          <groupId>org.xmlunit</groupId>
        </exclusion>
        <exclusion>
          <artifactId>hamcrest-core</artifactId>
          <groupId>org.hamcrest</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.github.jknack</groupId>
      <artifactId>handlebars</artifactId>
      <version>4.0.6</version>
      <scope>provided</scope>
      <exclusions>
        <exclusion>
          <artifactId>antlr4-runtime</artifactId>
          <groupId>org.antlr</groupId>
        </exclusion>
        <exclusion>
          <artifactId>rhino</artifactId>
          <groupId>org.mozilla</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.adobe.aem</groupId>
      <artifactId>uber-jar</artifactId>
      <version>6.3.0</version>
      <classifier>apis</classifier>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>2.8.4</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
      <version>2.8.4</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
      <version>2.8.4</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.osgi</groupId>
      <artifactId>org.osgi.annotation</artifactId>
      <version>6.0.0</version>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.annotation</groupId>
      <artifactId>javax.annotation-api</artifactId>
      <version>1.3.2</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>
</project>
